<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:fn="http://www.w3.org/2005/xpath-functions" exclude-result-prefixes="xs" version="2.0">



    <!-- Variables -->
    <xsl:variable name="imagePath" select="'content/xml/images/'"/>

    <!-- Custom Matches -->
    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- Clear up markup so we just have sections -->
    <xsl:template match="front">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="back">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="app-group">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- Remove width attribute from tables -->
    <xsl:template match="table/@width"/>

    <!-- Remove col from tables -->
    <xsl:template match="table/col"/>

    <!-- Remove col from tables -->
    <xsl:template match="table/colgroup"/>

    <!-- <alternatives> -->
    <xsl:template match="alternatives">
        <xsl:value-of select="."/>
    </xsl:template>

    <!-- <x> -->
    <xsl:template match="x">
        <xsl:text> </xsl:text>
        <xsl:value-of select="."/>
        <xsl:text> </xsl:text>
    </xsl:template>


    <!-- <italic> -->
    <xsl:template match="italic">
        <em>
            <xsl:value-of select="."/>
        </em>
    </xsl:template>

    <!-- <bold> -->
    <xsl:template match="bold">
        <strong>
            <xsl:value-of select="."/>
        </strong>
    </xsl:template>

    <!-- <def> -->
    <xsl:template match="def">
        <xsl:for-each select="p">
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>

    <!-- <std-organization> -->
    <xsl:template match="std-organization">
        <span class="std-organization">
            <xsl:value-of select="."/>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>

    <!-- <source> -->
    <xsl:template match="source">
        <span class="source">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <publisher-name> -->
    <xsl:template match="publisher-name">
        <span class="publisher-name">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <publisher-loc> -->
    <xsl:template match="publisher-loc">
        <span class="publisher-loc">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <surname> -->
    <xsl:template match="surname">
        <span class="surname">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <lpage> -->
    <xsl:template match="lpage">
        <span class="lpage">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <fpage> -->
    <xsl:template match="fpage">
        <span class="fpage">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <article-title> -->
    <xsl:template match="article-title">
        <span class="article-title">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <collab> -->
    <xsl:template match="collab">
        <span class="collab">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <volume> -->
    <xsl:template match="volume">
        <span class="volume">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <issue> -->
    <xsl:template match="issue">
        <span class="issue">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>


    <!-- <fn> -->
    <xsl:template match="fn">
        <div class="footnote">
            <xsl:attribute name="id" select="@id"/>

            <div class="footnote-label">
                <p>
                    <sup>
                        <xsl:for-each select="label">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </sup>
                </p>
            </div>
            <div class="footnote-content">
                <xsl:for-each select="p">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </div>
        </div>

    </xsl:template>

    <!-- <country> -->
    <xsl:template match="country">
        <span class="country">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <post-code> -->
    <xsl:template match="post-code">
        <span class="post-code">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <state> -->
    <xsl:template match="state">
        <span class="state">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <city> -->
    <xsl:template match="city">
        <span class="city">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <addr-line> -->
    <xsl:template match="addr-line">
        <span class="addr-line">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <label> -->
    <xsl:template match="label">
        <span class="label">
            <strong>
                <xsl:value-of select="."/>
            </strong>
        </span>
    </xsl:template>

    <!-- <given-names> -->
    <xsl:template match="given-names">
        <span class="given-names">
            <xsl:value-of select="."/>
        </span>
    </xsl:template>

    <!-- <uri> -->
    <xsl:template match="uri">
        <a>
            <xsl:attribute name="href" select="."/>
            <xsl:value-of select="."/>
        </a>
    </xsl:template>

    <!-- <title> -->
    <xsl:template match="title">
        <span class="title">
            <xsl:choose>
                <xsl:when test="inline-formula">
                    <xsl:value-of select="text()"/>
                    <xsl:apply-templates select="*"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:for-each select="node()">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>

    <!-- <break> -->
    <xsl:template match="break">
        <br/>
    </xsl:template>

    <!-- <graphic> -->
    <xsl:template match="graphic">
        <img>
            <xsl:attribute name="src"
                select="concat($imagePath, replace(replace(@xlink:href, '.tif', '.png'),'.eps','.png'))"/>
            <xsl:attribute name="width">65%</xsl:attribute>
     
        </img>
    </xsl:template>
    
    <!-- <graphic> -->
    <xsl:template match="inline-graphic">
        <img>
            <xsl:attribute name="src"
                select="concat($imagePath, replace(replace(@xlink:href, '.tif', '.png'),'.eps','.png'))"/>
            <xsl:attribute name="width">65%</xsl:attribute>
            
        </img>
    </xsl:template>

    <!-- <xref> -->
    <xsl:template match="xref">
        <xsl:variable name="ref-type">
            <xsl:value-of select="@ref-type"/>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$ref-type = 'table-fn'">
                <sup>
                    <a>
                        <xsl:attribute name="href">
                            <xsl:value-of select="@rid"/>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                    </a>
                </sup>
            </xsl:when>
            <xsl:when test="$ref-type = 'fn'">
                <sup>
                    <a>
                        <xsl:attribute name="href">
                            <xsl:value-of select="@rid"/>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                    </a>
                </sup>
            </xsl:when>
            <xsl:when test="$ref-type = 'disp-formula'">
                    <a>
                        <xsl:attribute name="id" select="@id"/>
                        <xsl:attribute name="href">
                            <xsl:value-of select="@rid"/>
                        </xsl:attribute>
                        <xsl:value-of select="."/>
                    </a>
                
            </xsl:when>
            <xsl:otherwise>
                <a>
                    <xsl:attribute name="href">
                        <xsl:value-of select="@rid"/>
                    </xsl:attribute>
                    <xsl:value-of select="."/>
                </a>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- <pub-id> -->
    <xsl:template match="pub-id">
        <span class="pub-id" pub-id-type="std-designation">
            <xsl:value-of select="."/>
            <xsl:text> - </xsl:text>
        </span>
    </xsl:template>

    <!-- <year> -->
    <xsl:template match="year">
        <span class="year">
            <xsl:value-of select="."/>
            <xsl:text> </xsl:text>
        </span>
    </xsl:template>

    <!-- <std> -->
    <xsl:template match="std">
        <span class="std">
            <xsl:for-each select="*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </span>
    </xsl:template>

    <!-- <ref> -->
    <xsl:template match="ref">
        <span class="ref">
            <xsl:attribute name="id" select="@id"/>
            <xsl:for-each select="*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </span>
        <br/>
    </xsl:template>

    <!-- <ref-list> -->
    <xsl:template match="ref-list">
        <xsl:for-each select="*">
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>


    <!-- <string-name> -->
    <xsl:template match="string-name">
        <span class="string-name">
            <xsl:for-each select="*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </span>
    </xsl:template>

    <!-- <person-group> -->
    <xsl:template match="person-group">
        <span class="person-group">
            <xsl:for-each select="*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </span>
    </xsl:template>

    <!-- <table-wrap-foot> -->
    <xsl:template match="table-wrap-foot">
        <span class="table-wrap-foot">
            <xsl:for-each select="*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </span>
    </xsl:template>

    <!-- <table-wrap> -->
    <!-- this is the element that wraps the title and footnotes of the table; as well as the table itself-->
    <xsl:template match="table-wrap">
        <!-- check if the table is in the footnotes; if it is, assign foot-note table  style class -->
        <xsl:choose>
            <xsl:when test="ancestor::fn">
                <div class="footnote-table">
                    <xsl:for-each select="*[not(self::label | title | caption)]">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </div>
            </xsl:when>
            <!-- if the table is not in the footnotes, it is part of the NESC content and gets the table-wrap style class -->
            <xsl:otherwise>
                <div class="table-wrap">
                    <xsl:attribute name="id" select="@id"/>
                    <!-- check if the table title needs break formatting -->
                    <xsl:choose>
                        <xsl:when test="caption/title/break">
                            <div class="table-title-break">
                                <!-- there are different table titles styles throughout the book; this choose statement assigns the appropriate formatting for each title based on the pdf. -->
                                <xsl:choose>
                                    <xsl:when
                                        test="@id = 'table124-1ma' or @id = 'table124-1mb' or @id = 'table124-1mc' or @id = 'table124-1md' or @id = 'table124-1fta' or @id = 'table124-1ftb' or @id = 'table124-1ftc' or @id = 'table124-1ftd'">
                                        <xsl:apply-templates select="label"/>
                                        <span class="dash">
                                            <xsl:text></xsl:text>
                                        </span>
                                        <br/>
                                        <xsl:for-each select="caption">
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:apply-templates select="label"/>
                                        <span class="dash">
                                            <xsl:text></xsl:text>
                                        </span>
                                        <xsl:for-each select="caption">
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </xsl:when>
                        <xsl:otherwise>
                            <div class="table-title">
                                <!-- there are different table titles styles throughout the book; this choose statement assigns the appropriate formatting for each title based on the pdf. -->
                                <xsl:choose>
                                    <xsl:when
                                        test="@id = 'table124-1ma' or @id = 'table124-1mb' or @id = 'table124-1mc' or @id = 'table124-1md' or @id = 'table124-1fta' or @id = 'table124-1ftb' or @id = 'table124-1ftc' or @id = 'table124-1ftd'">
                                        <xsl:apply-templates select="label"/>
                                        <span class="dash">
                                            <xsl:text></xsl:text>
                                        </span>
                                        <br/>
                                        <xsl:for-each select="caption">
                                            <xsl:apply-templates select="."/>
                                        </xsl:for-each>
                                    </xsl:when>
                                   
                                    <xsl:otherwise>
                                        
                                       
                                                <xsl:apply-templates select="label"/>
                                                <span class="dash">
                                                    <xsl:text></xsl:text>
                                                </span>
                                                
                                                <xsl:for-each select="caption">
                                                    <xsl:apply-templates select="."/>
                                                </xsl:for-each>
                                           
                                  
                                       
                                    </xsl:otherwise>
                                </xsl:choose>
                            </div>
                        </xsl:otherwise>
                    </xsl:choose>

                    <xsl:for-each select="*[not(self::label | title | caption)]">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </div>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- <list> -->
    <xsl:template match="list">
        <xsl:choose>
            <xsl:when test="list/label">
                <xsl:variable name="type" select="text()"/>
                <xsl:value-of select="$type"/>
                <ol>
                    <xsl:for-each select="*">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </ol>
            </xsl:when>
            <xsl:when test="list-item/label">
                <ol>
                    <xsl:for-each select="*">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </ol>
            </xsl:when>
            <xsl:otherwise>
                <ul>
                    <xsl:for-each select="*">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </ul>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- <mixed-citation> -->
    <xsl:template match="*/mixed-citation">
        <cite>
            <xsl:copy>
                <xsl:value-of select="."/>
            </xsl:copy>
        </cite>
    </xsl:template>

    <!-- <list-item> -->
    <xsl:template match="list-item">
        <xsl:choose>
            <xsl:when test="p/disp-formula">
                <div class='dispformulas'>
                    <xsl:variable name="type" select="label/text()"/>
                    <p class="label-nowrap">
                        <xsl:value-of select="$type"/>
                    </p>
                    <span class="list-item">
                        <xsl:for-each select="p">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </span>
                </div>
            </xsl:when>
            <xsl:otherwise>
                <div class="row list-item" style="display:-webkit-flex; display:flex">
                    <div class="col" style="max-width:3%">
                        <xsl:variable name="type" select="label/text()"/>
                        <p style="white-space: nowrap">
                            <xsl:value-of select="$type"/>
                        </p>
                    </div>
                    <div class="col" style="padding-left:10px;">
                        <xsl:for-each select="*[not(self::label)]">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </div>
                </div>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <!-- <fig> -->
    <xsl:template match="fig">
        <figure class="figure-tag" style="text-align:center;">
            <xsl:attribute name="id" select="@id"/>
            <xsl:apply-templates select="graphic"/>
            <br/>
            <xsl:apply-templates select="label"/>
            <xsl:if test="label">
                <xsl:text>&#8212;</xsl:text>
            </xsl:if>
            <p class="figure-text">
                <xsl:value-of select="caption/p/text()"/>
            </p>
        </figure>
    </xsl:template>
    
   

    <!-- <non-normative-note> -->
    <xsl:template match="non-normative-note">
        <div class="non-normative-note">
            <xsl:if test="ancestor::table-wrap">
                <xsl:attribute name="style" select="'padding-left:0px;'"/>
            </xsl:if>
            <em>
                <xsl:value-of select="label"/>
            </em>
            <xsl:text> </xsl:text>

            <xsl:choose>
                <xsl:when test="p/*">
                    <xsl:for-each select="p">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </xsl:when>

                <xsl:otherwise>
                    <xsl:value-of select="p/text()"/>
                </xsl:otherwise>
            </xsl:choose>
        </div>

    </xsl:template>

    <!-- <term> -->
    <xsl:template match="term">
        <xsl:choose>
            <xsl:when test="inline-formula/alternatives/tex-math">
                <div class='mathkatex terminline'><xsl:apply-templates select="replace(replace(replace(.,'\\\[',''),'\\\]',''),'\$','')"/></div>
                <!--<xsl:apply-templates select="inline-formula/tex-math"/>-->
            </xsl:when>
            <xsl:when test="italic/inline-formula/alternatives/tex-math">
                <div class='mathkatex terminline'><xsl:apply-templates select="replace(replace(replace(.,'\\\[',''),'\\\]',''),'\$','')"/></div>
            </xsl:when>   
            <xsl:otherwise>
                <dt>
                    <xsl:value-of select="."/>
                </dt>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
   

    <!-- <var-item> -->
    <xsl:template match="var-item">
        <span class="variable">
            <xsl:for-each select="*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </span>
    </xsl:template>

    <!-- <std-def-list-item> -->
    <xsl:template match="std-def-list-item">
        <p class="std-def-list-item">
            <xsl:choose>
                <xsl:when test="def/p/*">
                    <div class="std-deflist-item">
                    <strong>
                        <xsl:value-of select="term"/>
                    </strong>
                    <xsl:text>&#58; </xsl:text>
                    <xsl:for-each select="def/*[not(term)]">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                    </div>
                </xsl:when>
                
                <xsl:when test="related-term-group">
                    <div class="std-deflist-item">
                    <strong>
                        <xsl:value-of select="term"/>
                    </strong>
                    <xsl:text>&#58; </xsl:text>
                    <xsl:value-of select="related-term-group/italic"/>
                    <strong><xsl:value-of select="related-term-group/term"/></strong>
                    </div>
                </xsl:when>
                
                <xsl:when test="(def/p[not(*)])">
                    <div class="std-deflist-item">
                    <strong>
                        <xsl:value-of select="term"/>
                    </strong>
                    <xsl:text>&#58; </xsl:text>
                    <!--<xsl:value-of select="def"/>-->
                    <xsl:for-each select="def/*">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                    <br/>
                    </div>
                </xsl:when>
            </xsl:choose>
        </p>
    </xsl:template>
 
    <!-- <def-item> -->
    <xsl:template match="def-item">
        <p class="def-item">
            <xsl:choose>
                <xsl:when test="def/p/*">
                    <strong>
                        <xsl:value-of select="term"/>
                    </strong>
                    <xsl:text> </xsl:text>
                    <xsl:for-each select="def/*[not(term)]">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                    
                </xsl:when>
                
                <xsl:when test="def/p[not(*)]">
                    <strong>
                        <xsl:value-of select="term"/>
                    </strong>
                    <xsl:value-of select="def"/>
                    <br/>
                </xsl:when>
            </xsl:choose>
        </p>
    </xsl:template>

    <!-- <def-list> -->
    <xsl:template match="def-list">
        <xsl:choose>
            <xsl:when test="ancestor::front">
                <dl>
                    <xsl:for-each select="def-item">
                        <div class="row" style="padding-left:50px">
                            <div class="col-10 dl-term">
                                <xsl:value-of select="term/text()"/>
                                <xsl:if test="term/*">
                                    <xsl:apply-templates select="term/*"/>
                                </xsl:if>
                            </div>
                            <div class="col-40 dl-def">
                                <xsl:value-of select="def/p/text()"/>
                                <xsl:if test="def/p/*">
                                    <xsl:apply-templates select="def/p/*"/>
                                </xsl:if>
                            </div>
                        </div>
                    </xsl:for-each>
                </dl>
            </xsl:when>
            <xsl:otherwise>
                <dl class="content" style="padding-left:1em;">
                    <p>
                        <xsl:for-each select="*">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </p>
                </dl>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- <std-def-list> -->
    <xsl:template match="std-def-list">
        <xsl:choose>
            <xsl:when test="ancestor::front">
                <dl>
                    <xsl:for-each select="std-def-list-item">
                        <div class="row" style="padding-left:50px">
                            <div class="col-10 dl-term">
                                <xsl:value-of select="term/text()"/>
                                <xsl:if test="term/*">
                                    <xsl:apply-templates select="term/*"/>
                                </xsl:if>
                            </div>
                            <div class="col-90 dl-def">
                                <xsl:value-of select="def/p/text()"/>
                                <xsl:if test="def/p/*">
                                    <xsl:apply-templates select="def/p/*"/>
                                </xsl:if>
                            </div>
                        </div>
                    </xsl:for-each>
                </dl>
            </xsl:when>
            <xsl:otherwise>
                <dl class="content" style="padding-left:1em;">
                    <p>
                        <xsl:for-each select="*">
                            <xsl:apply-templates select="."/>
                        </xsl:for-each>
                    </p>
                </dl>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- <variable-list> -->
    <xsl:template match="variable-list">
        <xsl:for-each select="*">
            <xsl:apply-templates select="."/>
        </xsl:for-each>
    </xsl:template>

    <!-- <disp-formula> -->
    <xsl:template match="disp-formula">
        <xsl:choose>
            <xsl:when test="ancestor::app[contains(@id, 'appC')]">
                <xsl:for-each select="graphic">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <div class='mathforumula'>
                    <xsl:attribute name="id" select="@id"/>
                    <xsl:for-each select="child::*">                   
                       <xsl:choose>
                           <xsl:when test="tex-math">
                               <div class='mathkatex termmain'><xsl:apply-templates select="replace(replace(replace(.,'\\\[',''),'\\\]',''),'\$','')"/></div>
                           </xsl:when>
                           <xsl:otherwise>
                               <xsl:apply-templates select="."/>  
                           </xsl:otherwise>
                       </xsl:choose>  
                                        
                    
                    </xsl:for-each>
                </div>

            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    
    <!-- <inline-formula> -->
    <xsl:template match="inline-formula">
        <xsl:choose>
            <xsl:when test="ancestor::app[contains(@id, 'appC')]">
                <xsl:for-each select="graphic">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <div class='mathforumula'>
                    <xsl:attribute name="id" select="@id"/>
                    <xsl:for-each select="child::*">                   
                        <xsl:choose>
                            <xsl:when test="tex-math">
                                <div class='mathkatex termmain'><xsl:apply-templates select="replace(replace(replace(.,'\\\[',''),'\\\]',''),'\$','')"/></div>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:apply-templates select="."/>  
                            </xsl:otherwise>
                        </xsl:choose>  
                        
                        
                    </xsl:for-each>
                </div>
                
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <!-- First level children -->
    <xsl:template match="sec | app">
        <section>
            <xsl:attribute name="id" select="@id"/>
            <xsl:attribute name="sec-type" select="@sec-type"/>
            <xsl:variable name="appID" select="@id"/>
            <h3>
                <span class="label">
                    <xsl:value-of select="label"/>
                </span>
                <xsl:if test="annex-type">
                    <xsl:text> </xsl:text>
                    <span class="annex"><xsl:value-of select="annex-type"/></span>
                </xsl:if>
                <xsl:text> </xsl:text>
                <span class="title">
                    <xsl:for-each select="title/node()">
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </span>
            </h3>
            <xsl:for-each select="*">
                <xsl:apply-templates select="."/>
            </xsl:for-each>
        </section>
    </xsl:template>

    <!-- Second Level Children -->
    <xsl:template match="sec/sec | app/sec">
        <section class="subsection hidden padding-left sticky-header">
            <xsl:attribute name="id" select="@id"/>
            <div class="row row-no-padding ">
                <div class="col col-80 toggle-section">
                    <h4>
                        <span class="label">
                            <xsl:value-of select="label"/>
                        </span>
                        <xsl:text> </xsl:text>
                        <span class="title">
                            <xsl:for-each select="./title/node()">
                                <xsl:apply-templates select="."/>
                            </xsl:for-each>
                        </span>
                    </h4>
                </div>
                <div class="col text-right">
                    <button
                        class="button button-clear button-light icon ion-bookmark toggle-bookmark row-no-padding no-select-highlight"
                    />
                </div>
            </div>
            <div class="content">
                <xsl:for-each select="*">
                    <xsl:apply-templates select="."/>
                </xsl:for-each>
            </div>
        </section>
    </xsl:template>

    <!-- Trash -->
    <xsl:template match="sec/label | sec/title | app/label | app/title | app/annex-type | list-item/label"/>

    <!-- Catchall -->
    <xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
